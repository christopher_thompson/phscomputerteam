<?php
require_once '../php/Require.php';

echo html_begin_setup('about', login_check());
?>
<h3>About PHS Computer Team</h3>
<br />

<h4>Poolesville High School and PHS Computer Team</h4>
<p><a href="http://www.montgomeryschoolsmd.org/schools/poolesvillehs/">Poolesville High School</a> is an all-magnet high school located in Poolesville, MD (who would've guessed?). The school is divided into four magnet programs: Science, Math, and Computer Science (SMCS), Global Ecology, Humanities, and the Independent Studies Program (ISP). Poolesville HS was ranked the #1 High School in Maryland for the past two years and the #7 STEM school in the nation.</p>
<p>PHS Computer Team is just one of the many clubs available at Poolesville High School. Our team also has had very successful members, either qualifying in finals or winning several competitions, such as ACSL, CSAW HSF, DC3, and MDC3, with these competitions being at the national or international level.</p><br />

<h4>Christopher Thompson - Captain</h4>
<p>Hello there weary internet traveler, my name is Christopher Thompson and I am a senior at Poolesville High School in the Science, Math and Computer Science magnet program. I am a "co-captaining" with Carolina (see below) for the PHS Computer Team during the 2013 - 2014 school year. We both aim to make computer science more popular among both our peers in SMCS and people from other schools.</p><br />

<h4>Carolina Zarate - Captain</h4>
<p>Hi, my name is Carolina Zarate and I am currently a senior in the Science, Math, and Computer Science Program at Poolesville High School. I am one of the two captains for the 2013-2014 school year for PHS Computer Team. Our goal is to get more people interested in a wider array of computer science topics. My favorite area of computer science is anything to do with cyber security or digital forensics.</p><br />
<?php
echo html_end_setup();
?>
