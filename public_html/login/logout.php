<?php
require_once '../php/Require.php';


echo html_begin_setup('login', false);

if (login_check() == true) {
    $_SESSION = array();

    $params = session_get_cookie_params();

    setcookie(session_name(), '', time() - 42000, $params["path"], 
        $params["domain"], $params["secure"], $params["httponly"]);

    session_destroy();

    echo html_success("You were successfully logged out :D");
    echo html_info('<a href="../">Go home?</a>');
} else {
    echo html_error("You are not already logged in, so you might have a hard time logging out :D");
    echo html_info('<a href="./login.php">Login?</a>');
}
?>
</div>
</body>
<?php
echo html_end_setup();
?>
