<?php
require_once '../php/Require.php';

echo html_begin_setup('login', login_check());

if (login_check() != true) {
    if (isset($_GET['error'])) {
        echo html_error('Sorry, you could not be logged in :C');
    }
    echo <<<HTML
<link rel="stylesheet" href="../css/login/style.css">
<script type="text/javascript" src="../js/sha512.js"></script>
<script type="text/javascript" src="../js/forms.js"></script>
<div class="login-container login-body">
    <div class="login">
        <h1>Login</h1>
        <form action="process_login.php" method="post" name="login_form">
                <p>
                    <label for="login">Email:</label>
                    <input type="text" name="login" id="login" placeholder="name@example.com"></input>
                </p>
                <p>
                    <label for="password">Password:</label>
                    <input type="password" placeholder="Password" name="password" id="password"></input>
                </p>
                <p class="submit">
                    <input type="submit" value="login" onclick="formhash(this.form, this.form.password);" />
                </p>
        </form>
    </div>
</div>

HTML;
} else {
    echo html_error("You are already logged in, you have to log out first if you want to login again :P");
    echo html_info('<a href="./logout.php">Log out?</a>');
}

echo html_end_setup();
?>
