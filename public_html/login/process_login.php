<?php
require_once '../php/Require.php';


echo html_begin_setup('login', login_check());

if (login_check() != true) {
    if (isset($_POST['login'], $_POST['p'])) {
        $email = $_POST['login'];
        $password = $_POST['p'];

        if (login($email, $password) == true) {
            echo html_success('SUCCESSSSS!!!: You are now logged into this site :D');
            echo html_info('<a href="../">Start editing this site to your heart\'s content, unless you are a hacker, then plz do not do anything bad to this awesome site :C</a>');
        } else {
            echo html_error("Woah woah woah...something went wrong, you were not logged in :C");
            echo html_info('<a href="./">Try to login again?</a>');
        }
    } else {
        echo html_error("Woah woah woah...something went wrong, you were not logged in :C");
        echo html_info('<a href="./">Try to login again?</a>');
    }
} else {
    echo html_error("You are already logged in, you have to log out first if you want to login again");
    echo html_info('<a href="./logout.php">Log out?</a>');
}
?>
</div>
</body>
<?php
echo html_end_setup();
?>
