<?php
require_once '../php/Login.func.php';

sec_session_start();

if (login_check() == true) {
    header('Location: logout.php');
} else {
    header('Location: login.php');
}
?>
