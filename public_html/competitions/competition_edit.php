<?php
require_once '../php/Require.php';

$loggedIn = login_check();

echo html_begin_setup('competitions', $loggedIn);

if ($loggedIn == true) {
    if (!isset($_GET['id'])) {
        echo html_error('You have not selected a competition to edit :P');
    } else {
        $dbConn = new DatabaseConn();
        $dbConn->set_table('competitions');
        $id = $_GET['id'];

        if(!empty($_POST)) {
            $insert_id = $dbConn->edit_item(array($_POST['name'], $_POST['image'], $_POST['website'], $_POST['open_to'], $_POST['time_open'], $_POST['info']), $id);
            if($insert_id) {
                echo html_success("Competition changed. <a href='competition_view.php?id=$id'>View</a>");
            } else {
                echo html_error('There was a problem uploading the competition :C');
            }
        } 
        $competition = $dbConn->get_item($_GET['id']);

        if (!count($competition)) {
            echo html_error("Competition not found");
        } else {
            echo <<<HTML
                <div class="page-header"><h2>Edit the competition</h2></div>
                <form class="form" method="post">
                    <table>
                        <tr>
                            <td><label for="name">Competition Name</label></td>
                            <td><input type="text" name="name" id="name" value="{$competition['name']}" /></td>
                        </tr>
                        <tr>
                            <td><label for="image">Image</label></td>
                            <td><input type="text" name="image" id="image" value="{$competition['image']}"></input></td>
                        </tr>
                        <tr>
                            <td><label for="website">Website URL</label></td>
                            <td><input type="text" name="website" id="website" value="{$competition['website']}"></input></td>
                        </tr>
                        <tr>
                            <td><label for="open_to">Who is the competition open to?</label></td>
                            <td><input type="text" name="open_to" id="open_to" value="{$competition['open_to']}"></input></td>
                        </tr>
                        <tr>
                            <td><label for="time_open">When is this competition open?</label></td>
                            <td><input type="text" name="time_open" id="time_open" value="{$competition['time_open']}"></input></td>
                        </tr>
                        <tr>
                            <td><label for="info">Additional Information</label></td>
                            <td><textarea type="text" name="info" id="info">{$competition['info']}</textarea></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <input class="btn btn-primary" type="submit" value="LETS DO THIS!" />
                            </td>
                        </tr>
                    </table>
                </form>
                <form action="./index.php">
                    <button class="btn" style="margin-left:210px;">Nevermind :P</button>
                </form>
HTML;
        }
    }
} else {
    html_error('Haha I do not think so, you might want to check your cookies. Either 1) someone ate them or 2) they were never there in the first place :D');
}

echo html_end_setup();
?>
