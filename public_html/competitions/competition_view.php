<?php
require_once '../php/Require.php';

sec_session_start();

$loggedIn = login_check();

echo html_begin_setup('competitions', $loggedIn);

if (isset($_GET['id'])) {
    $id = $_GET['id'];

    $dbConn = new DatabaseConn($loggedIn);
    $dbConn->set_table('competitions');
    $competition = $dbConn->get_item($id);
    if (!count($competition)) {
        echo html_error("Competition #$id not found");
    } else {
        $name = $competition['name'];
        $image = $competition['image'];
        $website = $competition['website'];
        $open_to = $competition['open_to'];
        $time_open = $competition['time_open'];
        $info = html_entity_decode($competition['info']);

        echo <<<HTML
          <div class="competition" style="margin-right:20px">
        <h2>$name</h2><br/>
        <img class="pic" src=$image />
          <ul>
            <li><em>Website: </em><a href=$website>$website</a></li></br>
            <li><em>Open to: </em>$open_to</li></br>
            <li><em>Dates competition is open: </em>$time_open</li></br>
            <li><em>Additional info: </em>$info</li></br>
        </ul>
        </div>
HTML;
        if ($loggedIn == true) {
            echo "<a href='competition_edit.php?id=$id'>Edit</a> | <a href='competition_delete.php?id=$id'>Delete</a> | <a href='index.php'>View All Competitions</a>";
        } else {
            echo "<a href='index.php'>View All Competitions</a>";
        }
    }
} else { 
    echo html_error('You did not give me an id to select a competition :P');
}

echo html_end_setup();
?>
