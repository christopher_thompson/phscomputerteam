<?php
require_once '../php/Require.php';

$loggedIn = login_check();

echo html_begin_setup('competitions', $loggedIn);

$dbConn = new DatabaseConn($loggedIn);
$dbConn->set_table('competitions');
$competitions = $dbConn->get_all_items();

if ($loggedIn == true) {
    echo html_info('<br /><a class="btn btn-info" href="competition_add.php">Want to add another competition?</a><br/>');
}

if(!count($competitions)) {
    echo html_info("No competitions to see here");
} else {
    foreach ($competitions as $competition) {
        $id = $competition['id'];
        $name = $competition['name'];
        $image = $competition['image'];
        $info = nl2br(substr($competition['info'], 0, 300));
        echo <<<HTML
            <div class="competition">
                <img class="pic" src=$image />
                <h2>$name</h2>
                <p>$info...</p>
                <a href='competition_view.php?id=$id'>Read More</a>
            </div>
            <hr />
HTML;

    }
}

echo html_end_setup();
?>
