<?php
require_once '../php/Require.php';

sec_session_start();

$loggedIn = login_check();

echo html_begin_setup('competitions', $loggedIn);

if ($loggedIn == true) {
    if(!empty($_POST)) {
        $dbConn = new DatabaseConn($loggedIn);
        $dbConn->set_table('competitions');
        $insert_id = $dbConn->add_item(array($_POST['name'], $_POST['image'], $_POST['website'], $_POST['open_to'], $_POST['time_open'], $_POST['info']));
        if($insert_id) {
            echo html_success("Competition added. <a href='competition_view.php?id=$insert_id'>View</a>");
        } else {
            echo html_error('There was a problem uploading the competition :C');
        }
    }
    echo <<<HTML
    <div class="page-header"><h2>Add a competition</h2></div>
    <form class="form" method="post">
        <table>
            <tr>
                <td><label for="name">Competition Name</label></td>
                <td><input type="text" name="name" id="name" /></td>
            </tr>
            <tr>
                <td><label for="image">Competition Image</label></td>
                <td><input type="text" name="image" id="image" /></td>
            </tr>
            <tr>
                <td><label for="website">Website URL</label></td>
                <td><input type="text" name="website" id="website"></input></td>
            </tr>
            <tr>
                <td><label for="open_to">Who is the competition open to?</label></td>
                <td><input type="text" name="open_to" id="open_to"></input></td>
            </tr>
            <tr>
                <td><label for="time_open">When is this competition open?</label></td>
                <td><input type="text" name="time_open" id="time_open"></input></td>
            </tr>
            <tr>
                <td><label for="info">Additional Information</label></td>
                <td><textarea type="text" name="info" id="info"></textarea></td>
            </tr>
            <tr>
                <td></td>
                <td><input class="btn btn-primary" type="submit" value="LETS DO THIS!" /></td>
            </tr>
        </table>
    </form>
    <form action="./index.php">
        <button class="btn" style="margin-left:210px;">Nevermind :P</button>    
    </form>
HTML;

} else {
    echo html_error('The bad news is you are not logged in...the good news is you are awesome :D');
}

echo html_end_setup();
?>
