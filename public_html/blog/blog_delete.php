<?php
require_once '../php/Require.php';

$loggedIn = login_check();

echo html_begin_setup('blog', $loggedIn);

if($loggedIn == true) {
    if (isset($_GET['id'])) {
        $dbConn = new DatabaseConn($loggedIn);
        $dbConn->set_table('blog');
        $dbConn->delete_item($_GET['id']);
        $dbConn->redirect('index.php');
    } else {
        echo html_error('What were you planning on me doing if you didn\'t give me a number to delete? Come on now :D');
    }
} else {
    echo html_error('Please login, otherwise you cannot access the content on this page :C');
}

echo html_end_setup();
?>
</body>
