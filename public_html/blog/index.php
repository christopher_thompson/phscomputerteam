<?php
require_once '../php/Require.php';

$loggedIn = login_check();

echo html_begin_setup('blog', $loggedIn);

$dbConn = new DatabaseConn($loggedIn);
$dbConn->set_table('blog');

$blogs = $dbConn->get_all_items();

if($loggedIn == true) {
    echo html_info('<br /><a class="btn btn-info" href="blog_add.php">Hey there! Want to be more awesome and add to this here blog?</a><br/>');
} 
if(!$blogs) {
        echo 'No posts yet.';
} else {
    foreach ($blogs as $row) {
        $title = $row['title'];
        $id = $row['id'];
        $body = nl2br(substr($row['body'], 0, 300));
        echo <<<HTML
            <div class="blog">
                <h2>$title</h2>
                <div class="blogBody">$body...</div><br/>
                <a href='blog_view.php?id=$id'>Read More</a>
            </div>
            <hr/>
HTML;
    }
}

echo html_end_setup();
?>
