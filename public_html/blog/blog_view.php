<?php
require_once '../php/Require.php';

$loggedIn = login_check();

echo html_begin_setup('blog', $loggedIn);

if (!isset($_GET['id'])) {
    echo html_error("You didn't select any blog number :P");
} else {
    $dbConn = new DatabaseConn($loggedIn);
    $dbConn->set_table('blog');
    $blog = $dbConn->get_item($_GET['id']);
    $id = $_GET['id'];

    if (!$blog) {
            echo html_error("Blog #$id not found");
    } else {
        $title = $blog['title'];
        $date  = date('F j<\s\up>S</\s\up>, Y', $blog['date']);
        $body  = htmlspecialchars_decode(nl2br($blog['body']));

        echo <<<HTML
        <div class="blog">
        <h2>$title</h2>
        <em>Posted $date</em><br/>
        <div class="blogBody">$body</div><br/>
        </div>
HTML;

        if ($loggedIn == true) {
            echo "<a href='blog_edit.php?id=$id'>Edit</a> | <a href='blog_delete.php?id=$id'>Delete</a> | <a href='index.php'>View All Blogs</a>";
        } else {
            echo "<a href='index.php'>View All Blogs</a>";
        }
        echo "<hr/>";
    }
}

echo html_end_setup();
?>