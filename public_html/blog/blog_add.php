<?php
require_once '../php/Require.php';

$loggedIn = login_check();

echo html_begin_setup('blog', $loggedIn);

if($loggedIn == true) {
    if(!empty($_POST)) {
        $dbConn = new DatabaseConn($loggedIn);
        $dbConn->set_table('blog');
        $insert_id = $dbConn->add_item(array($_POST['title'], $_POST['body'], time()));
        if($insert_id) {
            echo html_success("Entry posted. <a href='blog_view.php?id=$insert_id'>View</a>");
        } else {
            echo html_error('There was a problem uploading your blog :C');
        }
    }
    echo <<<HTML
    <div class="page-header"><h2>Add a blog</h2></div>
    <form class="form" method="post">
        <table>
            <tr>
                <td><label for="title">Title</label></td>
                <td><input type="text" name="title" id="title" /></td>
            </tr>
            <tr>
                <td><label for="body">Body</label></td>
                <td><textarea type="text" name="body" id="body"></textarea></td>
            </tr>
            <tr>
                <td></td>
                <td><input class="btn btn-primary" type="submit" value="LETS DO THIS!" /></td>
            </tr>
        </table>
    </form>
    <form action="./index.php">
        <button class="btn" style="margin-left:35px;">Nevermind :P</button>    
    </form>
HTML;

} else {
    echo html_error('Please login, otherwise you cannot access the content on this page :C');
}
echo html_end_setup();
?>
