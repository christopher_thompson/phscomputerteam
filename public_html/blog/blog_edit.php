<?php
require_once '../php/Require.php';

$loggedIn = login_check();

echo html_begin_setup('blog', $loggedIn);

if($loggedIn == true) {
    if (!isset($_GET['id'])) {
        echo html_error('You have not selected a blog to edit :P');
    } else {
        $dbConn = new DatabaseConn($loggedIn);
        $dbConn->set_table('blog');
        $id = $_GET['id'];

        if(!empty($_POST)) {
            $insert_id = $dbConn->edit_item(array($_POST['title'], $_POST['body'], time()), $id);
            if($insert_id) {
                echo html_success("Blog added. <a href='blog_view.php?id=$id'>View</a>");
            } else {
                echo html_error("Something went wrong somewhere :C");
            }
        } 

        $blog = $dbConn->get_item($_GET['id']);

        if(!count($blog)) {
            echo html_error('Post not found');
        } else {
            echo <<<HTML
            <div class="page-header"><h2>Edit this blog</h2></div>
            <form class="form" method="post">
                <table>
                    <tr>
                        <td><label for="title">Title</label></td>
                        <td><input type="text" name="title" id="title" value="{$blog['title']}" /></td>
                    </tr>
                    <tr>
                        <td><label for="body">Body</label></td>
                        <td><textarea type="text" name="body" id="body">{$blog['body']}</textarea></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><input class="btn btn-primary" type="submit" value="LETS DO THIS!" /></td>
                    </tr>
                </table>
            </form>
            <form action="./index.php">
                <button class="btn" style="margin-left:35px;">Nevermind :P</button>    
            </form>
HTML;
        }
    }
} else {
    echo html_error('Please login, otherwise you cannot access the content on this page :C');
}

echo html_end_setup();
?>