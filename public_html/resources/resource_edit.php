<?php
require_once '../php/Require.php';



$loggedIn = login_check();

echo html_begin_setup('resources', $loggedIn);

if($loggedIn == true) {
    if (!isset($_GET['id'])) {
        echo html_error('You have not selected a resource to edit :P');
    } else {
        $dbConn = new DatabaseConn($loggedIn);
        $dbConn->set_table('resources');
        $id = $_GET['id'];

        if(!empty($_POST)) {
            $insert_id = $dbConn->edit_item(array($_POST['name'], $_POST['url'], $_POST['description']), $id);
            if($insert_id) {
                echo html_success("Resource added. <a href='resource_view.php?id=$id'>View</a>");
            } else {
                echo html_error("Something went wrong somewhere :C");
            }
        } 

        $resource = $dbConn->get_item($_GET['id']);

        if(!count($resource)) {
            echo html_error('Resource not found');
        } else {
            echo <<<HTML
            <div class="page-header"><h2>Edit this resource</h2></div>
            <form class="form" method="post">
                <table>
                    <tr>
                        <td><label for="name">Name: </label></td>
                        <td><input type="text" name="name" id="name" value="{$resource['name']}" /></td>
                    </tr>
                    <tr>
                        <td><label for="url">Website Url: </label></td>
                        <td><input type="text" name="url" id="url" value="{$resource['url']}"></input></td>
                    </tr>
                    <tr>
                        <td><label for="description">Description: </label></td>
                        <td><textarea type="text" name="description" id="description">{$resource['description']}</textarea></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><input class="btn btn-primary" type="submit" value="LETS DO THIS!" /></td>
                    </tr>
                </table>
            </form>
            <form action="./index.php">
                <button class="btn" style="margin-left:80px;">Nevermind :P</button>    
            </form>
HTML;
        }
    }
} else {
    echo html_error('Please login, otherwise you cannot access the content on this page :C');
}

echo html_end_setup();
?>