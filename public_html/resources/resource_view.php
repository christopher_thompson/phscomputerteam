<?php
require_once '../php/Require.php';



$loggedIn = login_check();

echo html_begin_setup('resources', $loggedIn);

if (!isset($_GET['id'])) {
    echo html_error("You didn't select any resource number :P");
} else {
    $dbConn = new DatabaseConn($loggedIn);
    $dbConn->set_table('resources');
    $resource = $dbConn->get_item($_GET['id']);
    $id = $_GET['id'];

    if (!$resource) {
            echo html_error("Resource #$id not found");
    } else {
        $name = $resource['name'];
        $url = $resource['url'];
        $description = html_entity_decode($resource['description']);

        echo <<<HTML
        <h2>$name</h2><br/>
        <ul>
            <li><em>Website: </em><a href=$url>$url</a></li></br>
            <li><em>Description: </em>$description</li></br>
        </ul>
HTML;
        if ($loggedIn == true) {
            echo "<a href='resource_edit.php?id=$id'>Edit</a> | <a href='resource_delete.php?id=$id'>Delete</a> | <a href='index.php'>View All Resources</a>";
        } else {
            echo "<a href='index.php'>View All Resources</a>";
        }
    }
}

echo html_end_setup();
?>
