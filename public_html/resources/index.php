<?php
require_once '../php/Require.php';

$loggedIn = login_check();

echo html_begin_setup('resources', $loggedIn);

$dbConn = new DatabaseConn($loggedIn);
$dbConn->set_table('resources');

$resources = $dbConn->get_all_items();

if($loggedIn == true) {
    echo html_info('<br /><a class="btn btn-info" href="resource_add.php">Want to add another resource?</a><br/>');
} 
if(!$resources) {
        echo 'No resources yet.';
} else {
    foreach ($resources as $row) {
        $name = $row['name'];
        $url = $row['url'];
        $description = nl2br(substr($row['description'], 0, 300));
        $id = $row['id'];
        echo <<<HTML
            <h2>$name</h2>
            <a href=$url>$url</a>
            <p>$description...</p>
            <a href='resource_view.php?id=$id'>Read More</a>
            <hr />
HTML;
    }
}

echo html_end_setup();
?>
