<?php
require_once '../php/Require.php';



$loggedIn = login_check();

echo html_begin_setup('resources', $loggedIn);

if($loggedIn == true) {
    if(!empty($_POST)) {
        $dbConn = new DatabaseConn($loggedIn);
        $dbConn->set_table('resources');
        $insert_id = $dbConn->add_item(array($_POST['name'], $_POST['url'], $_POST['description']));
        if($insert_id) {
            echo html_success("Resource posted. <a href='resource_view.php?id=$insert_id'>View</a>");
        } else {
            echo html_error('There was a problem uploading your resource :C');
        }
    }
    echo <<<HTML
    <div class="page-header"><h2>Add a resource</h2></div>
    <form class="form" method="post">
        <table>
            <tr>
                <td><label for="name">Name</label></td>
                <td><input type="text" name="name" id="name" /></td>
            </tr>
            <tr>
                <td><label for="url">Website Url</label></td>
                <td><input type="text" name="url" id="url"></input></td>
            </tr>
            <tr>
                <td><label for="description">Description</label></td>
                <td><textarea type="text" name="description" id="description"></textarea></td>
            </tr>
            <tr>
                <td></td>
                <td><input class="btn btn-primary" type="submit" value="LETS DO THIS!" /></td>
            </tr>
        </table>
    </form>
    <form action="./index.php">
        <button class="btn" style="margin-left:80px;">Nevermind :P</button>    
    </form>
HTML;

} else {
    echo html_error('Please login, otherwise you cannot access the content on this page :C');
}
echo html_end_setup();
?>