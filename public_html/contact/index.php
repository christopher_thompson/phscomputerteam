<?php
require_once '../php/Require.php';

echo html_begin_setup('contact', login_check());
?>
<h3>Contact PHS Computer Team</h3>
<br />
<p>Questions? Suggestions? More competitions to add? Saying hello? We can be reached in one of the following ways.</p><br />

<h4>Email</h4>
<p><img src="../images/mail2.png" style="max-width:25px;max-height:25px;margin:5px;vertical-align:middle" /><a href="mailto:phscomputerteam@gmail.com">phscomputerteam@gmail.com</a></p><br />

<h4>Twitter</h4>
<p><img src="../images/twitter.png" style="max-width:25px;max-height:25px;margin:5px;vertical-align:middle;" /><a href="https://twitter.com/PHSComputerTeam">https://twitter.com/PHSComputerTeam</a></p>
<?php
echo html_end_setup();
?>
