<?php
require_once 'DatabaseConn.class.php';

function sec_session_start() {
    $session_name = 'sec_session_id'; // Set a custom session name
    $secure = false; // Set to true if using https.
    $httponly = true; // This stops javascript being able to access the session id. 

    ini_set('session.use_only_cookies', 1); // Forces sessions to only use cookies. 
    $cookieParams = session_get_cookie_params(); // Gets current cookies params.
    session_set_cookie_params($cookieParams["lifetime"], $cookieParams["path"], $cookieParams["domain"], $secure, $httponly); 
    session_name($session_name); // Sets the session name to the one set above.
    session_start(); // Start the php session
    session_regenerate_id(true); // regenerated the session, delete the old one.
}

function login($email, $password) {
    $dbConn = new DatabaseConn(true, 'secure_login');
    $mysqli = $dbConn->get_conn();
    $email = $mysqli->real_escape_string($email);

    if ($stmt = $mysqli->prepare("SELECT id, username, password, salt FROM members WHERE email = ? LIMIT 1")) { 
        $stmt->bind_param('s', $email); // Bind "$email" to parameter.
        $stmt->execute(); // Execute the prepared query.
        $stmt->store_result();
        $stmt->bind_result($user_id, $username, $db_password, $salt); // get variables from result.
        $stmt->fetch();
        $password = hash('sha512', $password.$salt); // hash the password with the unique salt.
        $db_password = hash('sha512', $db_password.$salt);

        if($stmt->num_rows == 1) { // If the user exists
            // We check if the account is locked from too many login attempts
            if(checkbrute($user_id, $mysqli) == true) { 
                // Account is locked
                // Send an email to user saying their account is locked
                return false;
            } else {
                if($db_password == $password) { // Check if the password in the database matches the password the user submitted. 
                    // Password is correct!
                    $user_browser = $_SERVER['HTTP_USER_AGENT']; // Get the user-agent string of the user.

                    $user_id = preg_replace("/[^0-9]+/", "", $user_id); // XSS protection as we might print this value
                    $_SESSION['user_id'] = $user_id; 
                    $username = preg_replace("/[^a-zA-Z0-9_\-]+/", "", $username); // XSS protection as we might print this value
                    $_SESSION['username'] = $username;
                    $_SESSION['login_string'] = hash('sha512', $password.$user_browser);
                    // Login successful.
                    return true;    
                } else {
                    // Password is not correct
                    // We record this attempt in the database
                    $now = time();
                    $mysqli->query("INSERT INTO login_attempts (user_id, time) VALUES ('$user_id', '$now')");
                    return false;
                }
            }
        } else {
        // No user exists. 
        return false;
        }
    }
}

function checkbrute($user_id, $mysqli) {
    $now = time();
    $valid_attempts = $now - (2 * 60 * 60);

    if ($stmt = $mysqli->prepare("SELECT time FROM login_attempts WHERE user_id = ? AND time > '$valid_attempts'")) {
        $stmt->bind_param('i', $user_id);

        $stmt->execute();
        $stmt->store_result();

        if ($stmt->num_rows > 20) {
            return true;
        } else {
            return false;
        }
    }
}

function login_check() {
    $dbConn = new DatabaseConn(false, 'secure_login');
    $mysqli = $dbConn->get_conn();
    if (isset($_SESSION['user_id'], $_SESSION['username'], $_SESSION['login_string'])) {
        $user_id = $_SESSION['user_id'];
        $login_string = $_SESSION['login_string'];
        $username = $_SESSION['username'];

        $user_browser = $_SERVER['HTTP_USER_AGENT'];

        if ($stmt = $mysqli->prepare("SELECT password, salt FROM members WHERE id = ? LIMIT 1")) {
            $stmt->bind_param('i', $user_id);
            $stmt->execute();
            $stmt->store_result();
                
            if ($stmt->num_rows == 1) {
                $stmt->bind_result($password, $salt);
                $stmt->fetch();

                $login_check = hash('sha512', $password.$salt);                
                $login_check = hash('sha512', $login_check.$user_browser);
                
                if ($login_check == $login_string) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    } else {
        return false;
    }
}


?>
