<?php

/* *
 *  Generates an error popup for the given error message
 * */
function html_error ($error) {
    return "<div class='alert alert-error'><strong>Error </strong>$error</div>";
}

/* *
 *  Generates a success popup for the given success message
 * */
function html_success ($msg) {
    return "<div class='alert alert-success'><strong>Success </strong>$msg</div>";
}

/* *
 *  Generates an info popup for the given info message
 * */
function html_info ($info) {
    return "<div class='alert alert-info'><strong>Information </strong>$info</div>";
}

/* *
 * Generates the html meta data for all of the webpages
 * The title of the given page can be changed if need be
 * */
function html_meta_info ($title = null) {
    if ($title == null) {
        $title = "PHS Computer Team: We <3 Computers";
    }
    return <<<HTML
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
    <html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8"></meta>
        <title>$title</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport"></meta>
        <meta content="Where the fun begins." name="description"></meta>
        <meta content="Christopher Thompson" name="author"></meta>
        <link rel="shortcut icon" href="../images/favicon.ico"></link>
        <link type="text/css" rel="stylesheet" href="../css/bootstrap.css"></link>
        <link type="text/css" rel="stylesheet" href="../css/jquery-ui-1.10.3.custom.css"></link>
        <link type="text/css" rel="stylesheet" href="../css/main-style.css"></link>
        <link type="text/css" rel="stylesheet" href="../css/syntaxHighlighter_styles/shCoreMidnight.css"></link>
        <link type="text/css" rel="stylesheet" href="../css/syntaxHighlighter_styles/shThemeMidnight.css"></link>
    </head>
HTML;
}

/* *
 * The navigation bar for the webpage 
 * TODO Have the navigation bar scroll with the website
 * */
function html_nav_bar ($current_page, $loggedIn = false) {
    $page_names = array("competitions", "resources", "about",
        "blog", "contact", "login", "");
    if (!in_array($current_page, $page_names)) {
        error_log("Invalid page: $current_page");
    } else {
        $temp = $page_names;
        foreach ($temp as $name) {
            if ($name == $current_page) {
                $page_names[$name] = './';
            } else {
                $page_names[$name] = "../$name";
            }
        }
    }

    $login = $page_names['login'];
    $competitions = $page_names['competitions'];
    $resources = $page_names['resources'];
    $about = $page_names['about'];
    $blog = $page_names['blog'];
    $contact = $page_names['contact'];

    $login_str = 'Login';
    if ($loggedIn) {
        $login_str = 'Log Out';
    }

    return <<<HTML
    <div class="navbar">
      <div class="navbar-inner">
        <div class="container" style="width: auto;">
      <a class="brand" style="padding:0px;margin:0px;" href="../index.php"><img style="max-width:300px;padding:0px;position:relative;top:10px;" src="../images/Computer_Team_Logo_Small_Haxor.png" /></a>
          <div class="nav-collapse">
            <ul style="float:right;top:-8px" class="nav">
                <li><a class="nav-image" href="mailto:phscomputerteam@gmail.com"><img src="../images/mail2.png" /></a></li>
                <li><a class="nav-image" href="https://www.twitter.com/PHSComputerTeam"><img src="../images/twitter.png" /></a></li>
                <li><a class="nav-link" href="$login">$login_str</a></li>
                <li><a class="nav-link" href="$competitions">Competitions</a></li>
                <li><a class="nav-link" href="$blog">Blog</a></li>
                <li><a class="nav-link" href="$resources">Resources</a></li>
                <li><a class="nav-link" href="$about">About</a></li>
                <li><a class="nav-link" href="$contact">Contact</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
HTML;

}

/* *
 * Generates the html for the top of every webpage
 * */
function html_begin_setup ($page, $loggedIn = false, $title = null) {
    return html_meta_info($title) . "\n".
           '<body><div class="mainsection well">' .
           html_nav_bar($page, $loggedIn);
}

/* *
 * Generates the html for the end of every webpage
 * */
function html_end_setup () {
    $syntaxHighlighter = '';
    $handle = opendir("../js/syntaxHighlighter_scripts/");

    while (($file = readdir($handle)) !== false) {
        if ($file !== '..' AND $file !== '.') {
            $syntaxHighlighter .= "<script type='text/javascript' src='../js/syntaxHighlighter_scripts/$file'></script>\n";
        }
    }

    closedir($handle);
    return <<<HTML
</div>
<script type="text/javascript" src="../js/jquery-1.9.1.js"></script>
<script type="text/javascript" src="../js/jquery-ui-1.10.3.custom.min.js"></script>
<script type="text/javascript" src="../js/syntaxHighlighter_scripts/shCore.js"></script>
$syntaxHighlighter
<script type="text/javascript">
SyntaxHighlighter.config.bloggerMode = true;
SyntaxHighlighter.all();
</script>
</body>
</html>
HTML;
}

/* *
 * Converts the original blog into actual formatted blog stuffs
 * for displaying the content 
 * */
function blogify ($blog) {
    return $blog;
}
