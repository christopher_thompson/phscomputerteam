<?php
require_once 'DatabaseConn.vars.php';

class DatabaseConn
{
    /* *
     * The connection to the database given the DatabaseConn.vars
     * variables
     * */
    private $_conn;

    /* *
     * The name of the table which we want to get stuff from
     * */
    private $_table;

    /* *
     * The constructor which creates a new DatabaseConn object
     * */
    public function __construct ($admin = false, $database = null) {
        global $HOST;
        global $USER;
        global $PASSWORD;
        global $DATABASE;
        if ($database == null) {
            $database = $DATABASE;
        }

        if ($admin) {
            global $ADMIN_USER;
            global $ADMIN_PASSWORD;
            $this->_conn = new mysqli($HOST, $ADMIN_USER, $ADMIN_PASSWORD, $database);
            if ($this->_conn->connect_errno) {
                error_log("Failed to connect to MySQL: " . $this->_conn->connect_error);
            }
        } else {
            $this->_conn = new mysqli($HOST, $USER, $PASSWORD, $database);
            if ($this->_conn->connect_errno) {
                error_log("Failed to connect to MySQL: " . $this->_conn->connect_error);
            }
        }
    }

    // TODO Implement with mysqli->prepare()
    public function add_item ($values) {
        if (!$this->_table) {
            return false;
        }

        $table = $this->sanitize_variable($this->_table);
        $escaped_values = array();
        foreach ($values as $value) {
            array_push($escaped_values, $this->sanitize_variable($value));
        }
        $colNames = $this->get_column_names($table);
        $sql = "INSERT INTO $table (".join(',', $colNames) .
            ") VALUES ('".join("','", $escaped_values)."')";
        $this->_conn->query($sql);
        return $this->_conn->insert_id;
    }

    public function get_item ($id) {
        if (!$this->_table) {
            return false;
        }

        $id = intval($this->sanitize_variable($id));
        $table = $this->sanitize_variable($this->_table);
        $sql = "SELECT * FROM $table WHERE id = $id LIMIT 1";

        if (!$result = $this->_conn->query($sql)) {
            error_log("get item query error: $sql");
        }

        $item = $result->fetch_assoc();
        $result->free();
        return $item;
    }

    public function get_all_items () {
        if (!$this->_table) {
            return false;
        }

        $table = $this->sanitize_variable($this->_table);
        $sql = "SELECT * FROM $table ORDER BY id DESC";
        if (!$result = $this->_conn->query($sql)) {
            error_log("get all items query error: $sql");
            return array();
        }
        $items = array();
        while ($row = $result->fetch_assoc()) {
            array_push($items, $row);
        }
        return $items;
    }

    public function edit_item ($values, $id) {
        if (!$this->_table) {
            return false;
        }
        $table = $this->sanitize_variable($this->_table);
        $escaped_id = $this->sanitize_variable($id);
        $escaped_values = array();
        foreach ($values as $value) {
            array_push($escaped_values, $this->sanitize_variable($value));
        }

        $colNames = $this->get_column_names($table);
        $sqlCols = array();
        for ($i = 0; $i < count($colNames); $i++) {
            array_push($sqlCols, $colNames[$i]." = '".$escaped_values[$i]."'");   
        }

        $sql = "UPDATE $table SET ".join(', ', $sqlCols)." WHERE id=$escaped_id";
        return $this->_conn->query($sql);
    }

    public function delete_item ($id) {
        if (!$this->_table) {
            return false;
        }

        $escaped_id = $this->sanitize_variable($id);
        $table = $this->sanitize_variable($this->_table);
        $sql = "DELETE FROM $table WHERE id = ?";

        if ($stmt = $this->_conn->prepare($sql)) { 
            $stmt->bind_param('i', $id);
            $stmt->execute();
            return true;
        } else {
            if ($stmt == false) {
                error_log($this->_conn->error);
            } else {
                error_log($stmt->error);
            }
            return false;
        }
    }

    public function get_column_names($table) {
        $table_array = array(
            'blog' => array(
                'title', 'body', 'date'
            ),
            'competitions' => array(
                'name', 'image', 'website', 'open_to', 'time_open', 'info'
            ),
            'resources' => array(
                'name', 'url', 'description'
            )
        );
        return $table_array[$table];
    }

    //**********************************************************************************************
    //*****************Other seamingly meaningless functions****************************************
    //**********************************************************************************************

    public function sanitize_variable ($arg) {
        return htmlspecialchars($this->_conn->real_escape_string($arg));
    }

    public function redirect ($uri) {
        header('location:'.$uri);
        exit;
    }

    public function get_conn () {
        return $this->_conn;
    }

    public function set_table ($table) {
        $this->_table = $table;
    }
}
