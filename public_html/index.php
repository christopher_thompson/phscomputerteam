<?php
  require_once "php/Login.func.php";
  sec_session_start();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8"></meta>
    <title>PHS Computer Team: We <3 Computers</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport"></meta>
    <meta content="Where the fun begins." name="description"></meta>
    <meta content="Christopher Thompson" name="author"></meta>
    <link rel="shortcut icon" href="images/favicon.ico"></link>
    <link rel="stylesheet" href="css/bootstrap.css"></link>
    <link rel="stylesheet" href="css/jquery-ui-1.10.3.custom.css"></link>
    <link rel="stylesheet" href="css/main-style.css"></link>
    <script type="text/javascript" src="js/jquery-1.9.1.js"></script>
    <script type="text/javascript" src="js/jquery-ui-1.10.3.custom.js"></script>
  </head>
  <body>
  <div class="mainsection">
    <!--====Navigation Bar====-->
    <div class="navbar">
      <div class="navbar-inner">
        <div class="container" style="width: auto;">
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            <a href="index.php" class="brand" style="padding:0px;margin:0px;"><img style="max-width:300px;padding:0px;position:relative;top:15px;margin-bottom:6px;" src="images/Computer_Team_Logo_Small_Haxor.png" /></a>
            <div class="nav-collapse">
                <ul style="float:right;float:bottom;" class="nav">
                    <li><a class="nav-image" href="mailto:phscomputerteam@gmail.com"><img src="images/mail2.png" /></a></li>
                    <li><a class="nav-image" href="https://www.twitter.com/PHSComputerTeam"><img src="images/twitter.png" /></a></li>
                        <?php
                        if (login_check() == true) {
                            echo '<li><a class="nav-link" href="login/">Log Out</a></li>';
                        } else {
                            echo '<li><a class="nav-link" href="login/">Login</a></li>';
                        }
                    ?>
                    <li><a class="nav-link" href="competitions/">Competitions</a></li>
                    <li><a class="nav-link" href="blog/">Blog</a></li>
                    <li><a class="nav-link" href="resources/">Resources</a></li>
                    <li><a class="nav-link" href="about/">About</a></li>
                    <li><a class="nav-link" href="contact/">Contact</a></li>
                </ul>
            </div>
        </div>
      </div>
    </div>
    <!--====End Navigation Bar====-->
    <!--====Website Header====-->
    <!--====End Website Header====-->
    <div class="content">
    <!--====End Resource list====-->
    <div class="right-content">
    <!--====Competition list====-->
    <div class="competitions">
        <div class="well" style="text-align:center;padding-top:7px;">
        <div class="page-header" style="margin:0px;"><h3 style="text-align:left;margin:0px;">Competitions</h3></div>
        <?php
        $dbConn = new DatabaseConn(false);
        $dbConn->set_table('competitions');
        $competitions = $dbConn->get_all_items();

        if(!count($competitions)) {
            echo html_info("No competitions yet");
        } else {
            foreach ($competitions as $competition) {
                $id = $competition['id'];
                $name = substr($competition['name'], 0, 16);
                $image = $competition['image'];
            echo <<<HTML
              <a style="color:inherit" href="competitions/competition_view.php?id=$id">
                <div class="competition-link">
                    <figure>
                        <img class="pic" src=$image />
                        <figcaption>$name</figcaption>
                    </figure>
                </div>
             </a>
HTML;

            }
        }
        ?>
        </div>
    </div>
    </div>
    <!--====End Competition list====-->
    <div class="left-content">
      <img class="pic" style="width:96%;" src="images/pageBanner.png" />
    <!--====Blog list====-->
    <div class="blogs">
        <div class="page-header" style="margin-bottom:15px;margin-top:5px;"><h3 style="margin:5px;">PHS Computer Team Blog</h3></div>
        <div class="well">
        <?php
            require_once 'php/DatabaseConn.class.php';
            $dbConn = new DatabaseConn(false);
            $dbConn->set_table('blog');

            $blogs = $dbConn->get_all_items();
            if(!$blogs) {
                    echo 'No posts yet.';
            } else {
                $limit = 4;
                $i = 0;
                foreach ($blogs as $row) {
                    if ($i < $limit) {
                        $title = $row['title'];
                        $id = $row['id'];
                        $body = nl2br(substr($row['body'], 0, 300));
                        echo <<<HTML
                            <div class="blog">
                                <h2>$title</h2>
                                <div class="blogBody">$body...</div><br/>
                                <a href='blog/blog_view.php?id=$id'>Read More</a>
                            </div>
                            <hr/>
HTML;
                        $i++;
                    } else {
                        break;
                    }
                }
            }
        ?>
        </div>
    </div>
    <!--====End of Blog list====-->
    <!--====Resource list====-->
    <div class="resources">
        <div class="page-header"><h3>Resources to look at...</h3></div>
        <div class="well">
    <?php
        $dbConn = new DatabaseConn(false);
        $dbConn->set_table('resources');

        $resources = $dbConn->get_all_items();
        if(!$resources) {
                echo 'No resources yet.';
        } else {
            $limit = 4;
            $i = 0;
            foreach ($resources as $row) {
                if ($i < $limit) {
                    $name = $row['name'];
                    $url = $row['url'];
                    $description = nl2br(substr($row['description'], 0, 300));
                    $id = $row['id'];
                    echo <<<HTML
                        <h2>$name</h2>
                        <a href=$url>$url</a>
                        <p>$description...</p>
                        <a href='resources/resource_view.php?id=$id'>Read More</a>
                        <hr />
HTML;
                    $i++;
                } else {
                    break;
                }
            }
        }
    ?>
        </div>
    </div>
    <!--====End Resource List====-->
    <!--====Website intro message====-->
    <div class="page-header" style="border-bottom: 1px solid #444444"><h3>A little about this site...</h3></div>
    <div class="well" >
    <p>
    We are from Poolesville High School and we want to spread the love of Computer Science to everyone by providing resources and other detailed information
    about competition opportunities available to people interested in this field of study. Enjoy your stay and take a look at the blog for even more information.
    </p>
    </div><br />
    <!--====End Website intro message====-->
    </div> <!--====End of left side====-->
    </div> <!--====End of Content====-->
  </div>
  </body>
</html>
