<?php
require_once '../php/Require.php';

sec_session_start();

$loggedIn = login_check();

echo html_begin_setup('', $loggedIn);

if ($loggedIn == true) {
    echo "<h2>Images in this directory...</h2>";
    $handle = opendir("./");
    while (($file = readdir($handle)) !== false) {
        if ($file != 'index.php' AND $file != '.' AND $file != '..') {
            echo "<img class='pic' src='$file' />";
        }
    }
        
} else {
    header('Location: ../index.php');
}

echo html_end_setup();
?>
