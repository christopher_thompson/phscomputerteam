CREATE DATABASE phscomputerteam;

CREATE TABLE `phscomputerteam`.`blog` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `title` varchar(255) NOT NULL,
    `body` text NOT NULL,
    `num_comments` int(11) NOT NULL DEFAULT '0',
    `date` int(11) NOT NULL,
    PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `phscomputerteam`.`comments` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `post_id` int(11) NOT NULL,
    `name` varchar(255) NOT NULL,
    `email` varchar(255) NOT NULL,
    `website` varchar(255) DEFAULT NULL,
    `content` text NOT NULL,
    `date` int(11) NOT NULL,
    PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `phscomputerteam`.`competitions` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `name` text NOT NULL,
    `website` text NOT NULL,
    `open_to` text NOT NULL,
    `time_open` text NOT NULL,
    `info` text NOT NULL,
    `image` text NOT NULL,
    PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `phscomputerteam`.`resources` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `name` text NOT NULL,
    `url` text NOT NULL,
    `description` text NOT NULL,
    PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Setup login database and user with limited rights

CREATE USER 'sec_user'@'localhost' IDENTIFIED BY 'password';

CREATE USER 'admin_user'@'localhost' IDENTIFIED BY 'adminPassword';

GRANT SELECT ON `phscomputerteam`.`blog` TO 'sec_user'@'localhost';

GRANT SELECT, INSERT ON `phscomputerteam`.`comments` TO 'sec_user'@'localhost';

GRANT SELECT ON `phscomputerteam`.`competitions` TO 'sec_user'@'localhost';

GRANT SELECT ON `phscomputerteam`.`resources` TO 'sec_user'@'localhost';

GRANT SELECT, INSERT, UPDATE, DELETE ON `phscomputerteam`.`blog` TO 'admin_user'@'localhost';

GRANT SELECT, INSERT, UPDATE, DELETE ON `phscomputerteam`.`comments` TO 'admin_user'@'localhost';

GRANT SELECT, INSERT, UPDATE, DELETE ON `phscomputerteam`.`competitions` TO 'admin_user'@'localhost';

GRANT SELECT, INSERT, UPDATE, DELETE ON `phscomputerteam`.`resources` TO 'admin_user'@'localhost';

----------------------------------------------------------------------------
-- Will need to separetly call this sql as it will not run for some reason--
----------------------------------------------------------------------------

CREATE DATABASE secure_login;

CREATE TABLE `secure_login`.`members` (
    `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `username` VARCHAR(30) NOT NULL,
    `email` VARCHAR(50) NOT NULL,
    `password` CHAR(128) NOT NULL,
    `salt` CHAR(128) NOT NULL
) ENGINE = InnoDB;

CREATE TABLE `secure_login`.`login_attempts` (
    `user_id` int(11) NOT NULL,
    `time` VARCHAR(30) NOT NULL,
    `unlock_time` VARCHAR(30) NOT NULL
) ENGINE = InnoDB;

GRANT SELECT, INSERT, UPDATE ON `secure_login`.`members` TO 'sec_user'@'localhost';

GRANT SELECT ON `secure_login`.`login_attempts` TO 'sec_user'@'localhost';

GRANT SELECT, INSERT, UPDATE, DELETE ON `secure_login`.`members` TO 'admin_user'@'localhost';

GRANT SELECT, INSERT, UPDATE, DELETE ON `secure_login`.`login_attempts` TO 'admin_user'@'localhost';

-- Creating a test user with password 'demo' and salt 'demo'
INSERT INTO `secure_login`.`members` VALUES(1, 'test_user', 'test_user@demo.com', '26C669CD0814AC40E5328752B21C4AA6450D16295E4EEC30356A06A911C23983AAEBE12D5DA38EEEBFC1B213BE650498DF8419194D5A26C7E0A50AF156853C79', '26C669CD0814AC40E5328752B21C4AA6450D16295E4EEC30356A06A911C23983AAEBE12D5DA38EEEBFC1B213BE650498DF8419194D5A26C7E0A50AF156853C79');
