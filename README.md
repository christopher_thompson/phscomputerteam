phscomputerteam
===============
-------------------------------------------
Project Description
-------------------------------------------
A website for the Poolesville Computer team.
This website aims to make finding information about
computer science topics and competitions available to 
high school students easier.

-------------------------------------------
Project Dependencies
-------------------------------------------

You will need:
Apache Server Software
PHP
MySQL

The easiest way to get all these dependencies installed 
and setup is using some linux distro. 

If you are in Ubuntu you can run:

sudo apt-get install apache2

sudo apt-get install php5

sudo apt-get install php5-mysql

sudo apt-get install libapache2-mod-php5

sudo /etc/init.d/apache2 restart

Now to setup the Apache server so it is not running out of
your /var/www/ directory:

sudo cp /etc/apache2/sites-available/default /etc/apache2/sites-available/phscomputerteam

Open up the configuration file with some sort of text editor:

gksudo gedit /etc/apache2/sites-available/phscomputerteam

Change the "DocumentRoot" to point to the location of the source code you forked

Also you will need to change the "Directory" variable to the location of the forked code

Save the file and run:

sudo a2dissite default && sudo a2ensite phscomputerteam

sudo service apache2 restart

You can now open a browser and go to "127.0.1.1" to see the source code in action

-----------------------------------------------
Suggestions but not requrements
-----------------------------------------------

I would suggest you use Vim as your editor for this project for two reasons:

1. It is entirely customizable and it looks really nice if you change the colors around
2. There are an endless number of plugins that you can use for programming in any language

If you are interested in using Vim, my .vimrc file is included in the root of this project

in order to use my vimrc file and use vim, in Ubuntu run:

sudo apt-get install vim

cp <path to folder where vimrc file is>/.vimrc ~

I also suggest that you run:

vimtutor

In order to get acquainted with vim :D





