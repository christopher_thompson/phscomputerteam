set nocompatible
filetype off

set rtp+=~/.vim/bundle/vundle/
call vundle#rc()

Bundle 'gmarik/vundle'

Bundle 'tpope/vim-fugitive'
Bundle 'Lokaltog/vim-easymotion'
Bundle 'rstacruz/sparkup', {'rtp': 'vim/'}
Bundle 'tpope/vim-rails.git'
Bundle 'scrooloose/nerdtree.git'

filetype plugin indent on

"Changing default indent
set expandtab
set shiftwidth=2
set softtabstop=2

"Mapping keys
"Leader
let mapleader = ","
"PHP syntax checker
map <C-B> :!php -l $<CR>
"Switching between windows
nmap <silent> <C-K> :wincmd k<CR>
nmap <silent> <C-J> :wincmd j<CR>
nmap <silent> <C-H> :wincmd h<CR>
nmap <silent> <C-L> :wincmd l<CR>
"Setting Leader
let mapleader=","
"Map : to ; for faster commands
nmap ; :
"Opening Nerd Tree
nmap <silent> <leader>f :NERDTreeToggle<CR>
nmap fa za
nmap fr zR
imap jj <Esc>

set noswapfile

"Styling
colorscheme slate
set background=dark

set autoindent
set nu
